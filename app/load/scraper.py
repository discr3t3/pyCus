"""Main Collector of Content."""
import os
import urllib3
import certifi
import datetime
import multiprocessing
from app.config.scraper import BASE_DOWNLOAD, HEADER
from app.lib.logger import Logger


class Scraper:
    """Singleton Scraper for gathering web data."""

    __instance = None

    @staticmethod
    def instance():
        if not Scraper.__instance:
            raise Exception("Instance not instatiated.")
        return Scraper.__instance

    def __init__(self, time_instance):
        if Scraper.__instance:
            raise Exception("Singleton")
        else:
            self._time = time_instance
            self._header = HEADER
            self.http = urllib3.PoolManager(
                cert_reqs='CERT_REQUIRED',
                ca_certs=certifi.where(),
            )
            self.__add_directory()
            Scraper.__instance = self

    def __add_directory(self):
        """Create directory for storing downloads."""
        directory = BASE_DOWNLOAD + '/' + str(self._time)
        if not os.path.isdir(directory):
            os.makedirs(directory)

    @property
    def header(self):
        """
        Return current assigned header.

        POST: dict, current assigned header.
        """
        return self._header

    @header.setter
    def header(self, header):
        """
        Set header used in web requests.

        PRE: header, dict, dict of header elements.
        """
        self._header = header

    def response(self, host, request_type):
        """
        Make web response to desired host.

        PRE: host, String, location of host.
        PRE: request_type, String, GET/PUT/POST/DELETE;
        """
        return self.http.request(
            request_type,
            host,
            headers=self._header,
        )

    def get_url(self, url, file_name):
        """
        GET user suggested URL and store to file.

        PRE: url, String, url to be acquired.
        PRE: file_name, String, name of file data is sent to.
        POST: status, Integer, HTTP Status code.
        """

        time_now = datetime.datetime.now()
        time = time_now.strftime('%Y-%m-%d %H:%M:%S')
        request = self.response(url, 'GET')

        status = request.status
        msg = 'URL: ' + url + '; TIME: ' + time + '; STATUS ' + \
              str(status) + ';\n'
        Logger.log(msg)

        if status == 200:
            data_file = BASE_DOWNLOAD + '/' + str(self._time) + \
                '/' + file_name + '.txt'
            with open(data_file, 'w') as output:
                # Change this to file buffer?
                output.write(str(request.data))
                output.close()

        return status

    def acquire_urls(self, urls):
        """
        Acquire set of urls.
        PRE: urls, set, set of urls to acquire.
        """
        processes = []
        for url in urls:
            proc = multiprocessing.Process(
                target=self.get_url,
                args=(url['url'], url['name'])
            )
            proc.start()
            processes.append(proc)

        results = []
        for proc in processes:
            proc.join()
            results.append(proc.exitcode)

        return results
