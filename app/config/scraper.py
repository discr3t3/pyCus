"""CONFIG/REFERENCE ITEMS FOR SCRAPER"""


BASE_DOWNLOAD = '/var/www/html/pyCus/app/load/downloads' # for ubuntu;

HEADER = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': 1,
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '
                  'AppleWebKit/537.36 (KHTML, '
                  'like Gecko) Chrome/62.0.3202.94'
                  ' Safari/537.36'
}

SCRAPE_URLS = [
    {
        'url': 'ADD_URL_HERE',
        'name': 'ADD_FILE_NAME_HERE',
    },
]
