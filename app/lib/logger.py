from app.config.log import BASE, ERROR, ACCESS


class Logger:
    """
    Log all access/errors.

    TODO: Only open file buffer once;
    """

    @classmethod
    def log(cls, msg, log_type='INFO'):
        """Log access/errors to file."""
        log_file = ACCESS if log_type == 'INFO' else ERROR
        with open(BASE + log_file, 'a') as log:
            log.write(msg)
            log.close()
