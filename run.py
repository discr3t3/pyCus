"""Run population scripts."""


from app.load.scraper import Scraper
from app.config.scraper import SCRAPE_URLS
import time


# Gather data from URL first;
time_instance = time.time()
scraper = Scraper(str(time_instance)).instance()
output = scraper.acquire_urls(SCRAPE_URLS)